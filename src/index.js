miro.onReady(() => {
  const icon24 = '<svg width="24px" height="24px" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--gis" preserveAspectRatio="xMidYMid meet"><path d="M44.55 10.526C18.234 10.526 0 31.58 0 42.106s5.263 18.42 15.79 18.42c10.526 0 15.789 2.632 15.789 10.527c0 10.526 7.895 18.42 18.421 18.42c34.21 0 50-18.42 50-36.841c0-31.58-23.87-42.106-55.45-42.106zm-7.024 10.527a6.58 6.58 0 1 1 0 13.158a6.58 6.58 0 0 1 0-13.158zm21.053 0a6.58 6.58 0 1 1 0 13.158a6.58 6.58 0 0 1 0-13.158zm19.053 10.526a6.579 6.579 0 1 1 0 13.158a6.579 6.579 0 0 1 0-13.158zm-58.527 1.263a6.58 6.58 0 1 1 0 13.158a6.58 6.58 0 0 1 0-13.158zM54 63.158a7.895 7.895 0 0 1 7.895 7.895c0 4.36-5.535 7.894-9.895 7.894a7.895 7.895 0 0 1-7.895-7.894c0-4.36 5.535-7.895 9.895-7.895z" fill="currentColor"></path></svg>'

// TODO
// Add control pannel to select what types of cards are replaced
// Add option to hide default fields
// Use events to catch changes


  miro.initialize({
    extensionPoints: {
      bottomBar: {
        title: 'Hello World Nijin',
        svgIcon: icon24,
        onClick: async () => {

          let cards = await miro.board.widgets.get({type: 'card'})
          
          // Find which card widgets are JIRA Cards
          console.log("finding JIRA cards...")
          jira_cards = []
          for (var i = 0; i < cards.length; i++) {
            if ('card' in cards[i]) {
              if ('customFields' in cards[i].card) {
                // if (cards[i].card.customFields[1].value.includes('SP-')) {
                //   jira_cards.push(cards[i])
                // }
                // if (cards[i].card.customFields[1].value.includes('SDR-')) {
                //   jira_cards.push(cards[i])
                // }
                if (cards[i].card.customFields.length == 1) {
                  if (cards[i].card.customFields[0].value.includes('SPO-')) {
                    jira_cards.push(cards[i]);
                  }
                }
                else {
                  if (cards[i].card.customFields[1].value.includes('SPO-')) {
                    jira_cards.push(cards[i]);
                  }
                }
              }
            }
          }

          console.log("Updating JIRA cards...")
          for (var i = 0; i < jira_cards.length; i++) {
            if (jira_cards[i].card.customFields.length > 1) {
              await miro.board.widgets.update(
                {
                  id: jira_cards[i].id,
                  style: {backgroundColor: '#000000'},
                  card: {customFields: [jira_cards[i].card.customFields[1]]}
                }
                )
            }
          }
          console.log("Update complete.")

          // // Set background Color based on ticket type
          // for (var i = 0; i < jira_cards.length; i++) {
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes('Feature')) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //               style: {backgroundColor: '#6554C0'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes('Enabler')) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //               style: {backgroundColor: '#36B37E'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes('Spike')) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //               style: {backgroundColor: '#ff991f'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].value.includes('SDR-')) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //       style: {backgroundColor: '#ff0000'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes("Goal")) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //       style: {backgroundColor: '#36b37e'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes("Objective")) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //       style: {backgroundColor: '#2684ff'}})
          //   }
          //   if (jira_cards[i].card.customFields[1].iconUrl.includes("Epic")) {
          //     await miro.board.widgets.update({id: jira_cards[i].id, 
          //       style: {backgroundColor: '#6554c0'}})
          //   }
          // }


        },
      },
    },
  })
})
